import express, { Response, Request } from "express";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';

class Server extends BaseService {
  onPost(request: Request, response: Response): void {
    response.json(new ResponseText("Hello world !"));
  }
}

new Server(express()).startServer();