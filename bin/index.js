#!/usr/bin/env node
const readline = require("readline");
const fs = require("fs");
const fse = require("fs-extra");
const path = require("path");

const reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function patchPackageJson(serviceName, pathToFile) {
  const filePath = path.join(pathToFile, "package.json");
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      return console.log(err);
    }

    const result = data.replace(/\$SERVICE_NAME/g, serviceName);

    fs.writeFile(filePath, result, "utf8", function (err) {
      if (err) return console.log(err);
    });
  });
}

function createProject(source, destination, serviceName) {
  if (fs.existsSync(destination)) {
    console.log(`Folder ${destination} exists. Delete or use another name.`);
    return false;
  }

  fse.copy(source, destination, (err) => {
    if (err) {
      console.log("An error occured while copying the folder.");
      return console.error(err);
    }
    patchPackageJson(serviceName, destination);
    console.log("Done !");
    return true;
  });
}

function getProjectName(argv) {
  return new Promise((resolve, _reject) => {
    if (argv.length !== 0) {
      resolve(argv[0]);
    } else {
      reader.question("project name: ", (serviceName) => {        
        resolve(serviceName);
      });
    }
  });
}

getProjectName(process.argv.slice(2)).then((serviceName) => {
  const source = path.join(__dirname, "..", "template");
  const destination = path.join(process.cwd(), serviceName);
  console.log(`Trying to copy files here: ${destination}`);
  createProject(source, destination, serviceName);
  reader.close();
  return 0;
});
